import { Component, Input } from '@angular/core';
import { ProfileData } from 'src/app/services/profile.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent {
  @Input() profileData: ProfileData;
}
