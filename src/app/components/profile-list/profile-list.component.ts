import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, Input, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { Subject, Observable } from 'rxjs';
import { Router } from '@angular/router'; 

import { compare } from 'src/app/utils/utils';
import { ProfileService, ProfileData } from 'src/app/services/profile.service';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() public showFilter: boolean;
  @Input() public filterValue: Observable<string>;
  MAX_PROFILES: number = 300;
  displayedColumns: String[] = ['photo', 'localid', 'email', 'first_name', 'phone', 'address', 'modified', 'view'];
  profiles$: Subject<ProfileData[]>; 
  paginatedData: MatTableDataSource<ProfileData>;
  resultsLength: Number = 0;

  showNoResults: boolean = false;
  noResults: string = 'No records found.'

  private paginator: MatPaginator;

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {    
    if (this.resultsLength === 0) return;
    
    this.paginator = mp || null;
    this.paginatedData.paginator = this.paginator || null;
  }
  
  constructor(
    private profileService: ProfileService,
    private ref: ChangeDetectorRef,
    private router: Router
  ) {
    this.profileService.getProfiles();
  }

  ngOnInit() {
    this.profiles$ = this.profileService.profilesResults;
    this.profiles$.subscribe((data: ProfileData[]) => {
      data = data.slice(0, this.MAX_PROFILES);
      this.resultsLength = data.length;
      this.paginatedData = new MatTableDataSource<ProfileData>(data);
    });
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  ngOnChanges(): void {
    if (!this.filterValue) return;
    this.filterValue.subscribe(val => {
      this.paginatedData.filter = val.trim().toLowerCase();
      this.showNoResults = (this.paginatedData.filteredData.length <= 0) ? true : false; 
    })
  }

  onRowClicked(row: ProfileData) {
    this.router.navigate(['/user-detail', row.localid]);
  }

  sortData(sort: Sort) {
    const data = this.paginatedData.data.slice();

    if (!sort.active || sort.direction === '') {
      this.paginatedData.data = data;
      return;
    }

    this.paginatedData.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'localid': return compare(a.localid.toString(), b.localid.toString(), isAsc);
        case 'email': return compare(a.email, b.email, isAsc);
        case 'first_name': return compare(a.first_name, b.first_name, isAsc);
        case 'phone': return compare(a.phone, b.phone, isAsc);
        case 'address': return compare(a.address, b.address, isAsc);
        case 'modified': return compare(a.modified.toString(), b.modified.toString(), isAsc);
        default: return 0;
      }
    });
  }
}
