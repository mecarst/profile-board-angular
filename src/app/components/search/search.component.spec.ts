import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { ProfileService } from '../../services/profile.service';

import { SearchComponent } from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ProfileService],
      declarations: [ SearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an input', () => {
    const input = fixture.nativeElement.querySelector('input')
    expect(!!input).toBe(true);
  });
  it('should have an button', () => {
    const button = fixture.nativeElement.querySelector('button')
    expect(!!button).toBe(true);
  });

  it('should trigger searchPhotos', () => {
    spyOn(component, 'searchPhotos');
    component.searchPhotos();
    expect(component.searchPhotos).toHaveBeenCalled();
  });
});
