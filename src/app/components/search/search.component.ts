import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {  
  @Output() filterValue: EventEmitter<string> = new EventEmitter<string>();
  profiles = new FormControl('', [Validators.required]);
  errorMsg: string = 'Profiles not found';

  searchProfiles(): void {
    if (!this.profiles.valid || this.profiles.value === '') {
      return;
    };
  }

  applyFilter($event: Event) {
    this.filterValue.emit(($event.target as HTMLInputElement).value.trim().toLowerCase());
  }
}
