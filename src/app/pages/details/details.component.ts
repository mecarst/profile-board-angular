import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 
import { Observable } from 'rxjs';

import { ProfileService, ProfileData } from 'src/app/services/profile.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  profile$: Observable<ProfileData[]>; 
  profile: ProfileData;

  constructor(
    private route: ActivatedRoute,
    private profileService: ProfileService,
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.getParams(+params['id']));
  }

  getParams(param: Number): void {
    this.profile$ = this.profileService.getProfileById(param);
    this.profile$.subscribe((profile: ProfileData[]) => {
      if (profile.hasOwnProperty('error') || profile.length <= 0) return;
      this.profile = this.profileService.filterBasedOnId(profile, param)[0];
    });
  }

}
