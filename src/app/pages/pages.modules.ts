import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';

import { HeaderComponent } from '../components/header/header.component';
import { NotFoundComponent } from './error/error.component';
import { DetailsComponent } from './details/details.component';
import { QuickFactsComponent } from '../components/quick-facts/quick-facts.component';
import { SearchComponent } from '../components/search/search.component';
import { HomeComponent } from '../pages/home/home.component';
import { ProfileListComponent } from '../components/profile-list/profile-list.component';
import { TabsComponent } from '../components/tabs/tabs.component';

const COMPONENTS = [
  HomeComponent,
  ProfileListComponent,
  HeaderComponent,
  QuickFactsComponent,
  NotFoundComponent,
  DetailsComponent,
  SearchComponent,
  TabsComponent
]

@NgModule({
  declarations: COMPONENTS,
  exports: COMPONENTS,
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTabsModule,
    MatCardModule,
    MatSortModule,
    MatListModule
  ]
})
export class PagesModule { }
