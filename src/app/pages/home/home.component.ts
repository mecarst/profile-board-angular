import { Component, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  filterValObs: Observable<string>;

  constructor(
    private profileService: ProfileService
  ) {
  }

  filterVal($event: string) {
    this.filterValObs = this.profileService.filterVal($event);
  }
}
