import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Subject, Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { error } from 'protractor';

export interface ProfileData {
  address: string;
  birthdate: Date;
  email: string;
  email2: string;
  first_name: string;
  gender: string;
  last_name: string;
  localid: Number;
  loyalty_member_id: string;
  modified: Date
  phone: string;
  photo: string;
  prefix: string;
  suffix: string;
}

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  profileUrl = environment.profileUrl;
  profilesResults = new Subject<ProfileData[]>();
  response = this.profilesResults.asObservable();
  tempData: Array<ProfileData[]> = [];
  filterValue: Subject<string> = new Subject<string>();

  constructor(
    private http: HttpClient
  ) {}

  getProfiles() {
    this.http.get<ProfileData[]>(`https://${this.profileUrl}`)
    .pipe(catchError((err: Error) => of(err)))
    .subscribe(
      (data: ProfileData[]) => {
        this.tempData.push(data);
        this.profilesResults.next(data);
      }
    );
  }

  getProfileById(profileId: Number): Observable<ProfileData[]> {
    if (this.tempData.length <= 0) {
      this.getProfiles();
      return this.profilesResults;
    }
    return of(this.filterBasedOnId(this.tempData[0], profileId));
  }

  filterBasedOnId(data: ProfileData[], profileId: Number): ProfileData[] {
    return data.filter((profile: ProfileData) => (profileId === profile.localid));
  }

  filterVal(value: string): Observable<string> {
    return of(value);
  }
}
